----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    20:05:54 07/12/2022 
-- Design Name: 
-- Module Name:    ContadorCiclo7 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;

entity ContadorCiclo7 is
Port ( Clk, Reset : in  STD_LOGIC;
           Q : out  STD_LOGIC_VECTOR (6 downto 0));
end ContadorCiclo7;

architecture Behavioral of ContadorCiclo7 is
signal aux : STD_LOGIC_VECTOR (6 downto 0) := "0000000";
begin
	process (Clk, Reset)
	begin
		if (Clk'event and Clk = '1') then				
			if (Reset = '0' or Aux = "1000001") then
				Aux <= "0000000";
			elsif (Aux = "0000000") then
				Aux <= Aux + 1;
			else
				Aux <= Aux + 2;
			end if;
		end if;
		Q <= Aux;
	end process;

end Behavioral;

