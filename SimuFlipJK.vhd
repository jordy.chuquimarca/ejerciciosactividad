--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   18:53:34 07/12/2022
-- Design Name:   
-- Module Name:   C:/Users/Pc/Downloads/Telegram Desktop/Memorias/ActividadEjercicios/SimuFlipJK.vhd
-- Project Name:  ActividadEjercicios
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: FlipFlopJK
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY SimuFlipJK IS
END SimuFlipJK;
 
ARCHITECTURE behavior OF SimuFlipJK IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT FlipFlopJK
    PORT(
         J : IN  std_logic;
         K : IN  std_logic;
         CLOCK : IN  std_logic;
         Q : INOUT  std_logic;
         Qn : INOUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal J : std_logic := '0';
   signal K : std_logic := '0';
   signal CLOCK : std_logic := '0';

	--BiDirs
   signal Q : std_logic;
   signal Qn : std_logic;

   -- Clock period definitions
   constant CLOCK_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: FlipFlopJK PORT MAP (
          J => J,
          K => K,
          CLOCK => CLOCK,
          Q => Q,
          Qn => Qn
        );

   -- Clock process definitions
   CLOCK_process :process
   begin
		CLOCK <= '0';
		wait for CLOCK_period/2;
		CLOCK <= '1';
		wait for CLOCK_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	

      wait for CLOCK_period*10;

      -- insert stimulus here 

      wait;
   end process;

END;
