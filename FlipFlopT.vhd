----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    19:03:57 07/12/2022 
-- Design Name: 
-- Module Name:    FlipFlopT - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
Library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity FlipFlopT is
Port ( Clk, T : in  STD_LOGIC;
       Q, Qn : inout  STD_LOGIC);
end FlipFlopT;

architecture Behavioral of FlipFlopT is
begin
	process (Clk, T)
			begin
				if (Clk'event and Clk = '1') then
					if (T = '1') then
						Q <= not Qn;
						Qn <= not Q;				
					else 
						Q <= Q;
						Qn <= Qn;
					end if;
				end if;
		end process;
end Behavioral;