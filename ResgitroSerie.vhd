----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    19:30:16 07/12/2022 
-- Design Name: 
-- Module Name:    ResgitroSerie - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.std_logic_unsigned.all;


entity RegistroSerie is
 Port ( Clk, MC : in  STD_LOGIC;
           InSerie : out STD_LOGIC_VECTOR (3 downto 0);
           Q : inout  STD_LOGIC_VECTOR (3 downto 0));
end RegistroSerie;

architecture Behavioral of RegistroSerie is
begin
process (Clk, MC)
	begin
		Q <= "0000";
		if (Clk'event and Clk = '0') then
			if (MC = '0') then
				InSerie(3) <= ((Q(3) nor Q(2)) nor Q(1));
			end if; 
		end if;
	end process;
end Behavioral;

