--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   19:39:12 07/12/2022
-- Design Name:   
-- Module Name:   C:/Users/Pc/Downloads/Telegram Desktop/Memorias/ActividadEjercicios/SimuRegistroSerie.vhd
-- Project Name:  ActividadEjercicios
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: RegistroSerie
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY SimuRegistroSerie IS
END SimuRegistroSerie;
 
ARCHITECTURE behavior OF SimuRegistroSerie IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT RegistroSerie
    PORT(
         Clk : IN  std_logic;
         MC : IN  std_logic;
         InSerie : OUT  std_logic_vector(3 downto 0);
         Q : INOUT  std_logic_vector(3 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal Clk : std_logic := '0';
   signal MC : std_logic := '0';

	--BiDirs
   signal Q : std_logic_vector(3 downto 0);

 	--Outputs
   signal InSerie : std_logic_vector(3 downto 0);

   -- Clock period definitions
   constant Clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: RegistroSerie PORT MAP (
          Clk => Clk,
          MC => MC,
          InSerie => InSerie,
          Q => Q
        );

   -- Clock process definitions
   Clk_process :process
   begin
		Clk <= '0';
		wait for Clk_period/2;
		Clk <= '1';
		wait for Clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	

      wait for Clk_period*10;

      -- insert stimulus here 

      wait;
   end process;

END;
