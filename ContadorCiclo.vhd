----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    19:50:46 07/12/2022 
-- Design Name: 
-- Module Name:    ContadorCiclo - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;

entity ContadorCiclo is
Port ( Clk, Reset : in  STD_LOGIC;
           Q : out  STD_LOGIC_VECTOR (2 downto 0));
end ContadorCiclo;

architecture Behavioral of ContadorCiclo is
signal aux : STD_LOGIC_VECTOR (2 downto 0) := "000";
begin
	process (Clk, Reset)
	begin
		if (Clk'event and Clk = '1') then				
			if (Reset = '0' or Aux = "101") then
				Aux <= "000";
			elsif (Aux = "000") then
				Aux <= Aux + 1;
			else
				Aux <= Aux + 2;
			end if;
		end if;
		Q <= Aux;
	end process;

end Behavioral;

